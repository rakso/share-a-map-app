# Share a Map app

Simple web application written mainly with [ChatGPT](https://chat.openai.com/chat) that parses an `.xlsx` file containing user name, user info and user address and then displays the user's location on a map - which can be shared.

_This application uses data from the great [OpenStreetMap](https://www.openstreetmap.org/about)._

## Getting started

1. Upload the `index.html` on the web server.
1. Prepare Excel file.
   1. File should have a header (first row is ignored)
   1. Data should be in columns like "**User Name**" "**User Info**" "**Valid Address String**".
1. In the application click on `select file` - wait a bit, after file is successfully loaded you'll get a notification. _The file won't be sent to the Internet. It stays on your computer._
1. You can select users and see them on the map!

### Sample spreadsheet view

| User name | User info | Address                          |
| --------- | --------- | -------------------------------- |
| John      | Doe       | Kuracyjna, 84-150 Hel            |
| Alice     |           | Przestrzenna 10, 70-800 Szczecin |
